.PHONY: all
all: *.adoc
	asciidoctor *.adoc

.PHONY: clean
clean:
	rm *.html
