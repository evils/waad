{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  nativeBuildInputs = with pkgs; [ asciidoctor ];
  shellHook = ''
    exec zsh
  '';
}
